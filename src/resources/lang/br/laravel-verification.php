<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel 2 Step Verification Language Lines
    |--------------------------------------------------------------------------
    */
    'title'                         => 'Verificação em 2 etapas',
    'subtitle'                      => 'Verificação Requerida.<br>Um e-mail foi enviado com o código de acesso.',
    'titleFailed'                   => 'Falha na Verificação',
    'titlePassed'                   => 'Boas notícias, pessoal!',

    'inputAlt1'                     => 'Entrada de código 1',
    'inputAlt2'                     => 'Entrada de código 2',
    'inputAlt3'                     => 'Entrada de código 3',
    'inputAlt4'                     => 'Entrada de código 4',

    'verifyButton'                  => 'Verificar',
    'attemptsRemaining'             => 'Tentativa restante | Tentativas restantes',
    'missingCode'                   => 'Não recebeu código de verificação?',

    'exceededTitle'                 => 'Tentativas de verificação excedidas',
    'lockedUntil'                   => 'Conta bloqueada até:',
    'tryAgainIn'                    => 'Tente novamente em',
    'returnButton'                  => 'Voltar para Início',

    'verificationEmailSubject'      => 'Verificação Necessária',
    'verificationEmailGreeting'     => 'Olá :username',
    'verificationEmailMessage'      => 'A WeGet2U recebeu uma solicitação de acesso através do e-mail <strong>:email</strong>.<br><br>Use este código para concluir o acesso de sua conta:<br><br><center><h1>:code</h1></center><br>O código tem validade de 24 horas.<br><br>Se você não reconhece esta solicitação, alguém pode estar tentando acessar a sua conta. Altere imediatamente a senha de sua conta ou entre em contato com nosso suporte pelo <strong>WhatsApp</strong> através desse <a href="https://api.whatsapp.com/send?phone=5511937293951">link</a>.<br><br>Se precisar, estamos aqui para te ajudar.',
    'verificationEmailButton'       => 'Verifique agora',

    'verificationEmailSuccess'      => 'Sucesso!',
    'verificationEmailSentMsg'      => 'E-mail de verificação enviado!',

    'verificationWarningTitle'      => 'Aviso!',
    'verificationWarningMessage'    => 'Esta é sua última tentativa antes de sua conta ser bloqueada por :hours horas.',

    'verificationLockedTitle'       => 'Doh!',
    'verificationLockedMessage'     => 'Conta bloqueada!',

    'verificationModalConfBtn'      => 'Ok',

];
